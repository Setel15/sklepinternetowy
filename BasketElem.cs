﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklepInternetowy
{
    public class BasketElem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public double Prize { get; set; }

        public BasketElem(string id, string name, int number, double prize)
        {
            Id = id;
            Name = name;
            Number = number;
            Prize = prize;
        }
    }
}