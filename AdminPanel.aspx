﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminPanel.aspx.cs" Inherits="sklepInternetowy.AdminPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        #panel{
            text-align:center;
        }
      #panel > a{
          margin: 20px;
      }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1><asp:Label runat="server" ID="lb404">404 Page Not Found</asp:Label></h1>
            <asp:ListView ID="ListView1" runat="server"></asp:ListView>
            <asp:Panel ID="panel" runat="server"> </asp:Panel>
            <a href="index.aspx">Go back to main page</a>
        </div>
    </form>
</body>
</html>
