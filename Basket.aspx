﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Basket.aspx.cs" Inherits="sklepInternetowy.Basket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link rel="stylesheet"  href="bootstrap/vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet"  href="assets/css/index.css" />
    <title></title>
    <style>
        #container{
            margin-top: 200px;
            display: flex;
            flex-direction: column;
            margin-left: 30px;
        }
        .elem{
            width: 80%;
            height: 100px;
            border: 1px solid black;
            margin: 20px;
            float: left;
            text-align:left;
            display: flex;
            flex-direction:column;
        }
        #btBuy{
           margin-left: 0 auto;
           width: 100px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <div class="navbar-brand">Shop Name</div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.aspx">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForMen.aspx">For Men</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForWomen.aspx">For Women</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForChildren.aspx">For Children</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Sale.aspx">Sale</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Contact.aspx">Contact</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="Basket.aspx">Basket</a>
                    </li>
                    <li class="nav-item">
                        <asp:Button CssClass="nav-link" BackColor="#343a40" ID="btToLogin" runat="server" OnClick="toLogin" Text="Sign in|Sign up" BorderStyle="None"></asp:Button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="container">
        <h1><asp:Label ID="lbTitle" runat="server"></asp:Label></h1>
        <asp:Panel ID="data" runat="server"></asp:Panel>
        <asp:Button ID="btBuy" runat="server" OnClick="btBuy_Click" Text="Buy" Visible="false" />
    </div>
    </form>
    <script src="bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
