﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace sklepInternetowy
{
    public partial class Basket : System.Web.UI.Page
    {
        public double totalAmmount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["basket"] == null)
            {
                lbTitle.Text = "Your basket is empty";
            }
            else
            {
                btBuy.Visible = true;
                lbTitle.Text = "Your basket:";
                List<BasketElem> basket = (List<BasketElem>)Session["basket"];
                foreach (BasketElem elem in basket)
                {
                    HtmlGenericControl div = new HtmlGenericControl("div");
                    div.Attributes["class"] = "elem";
                    HtmlGenericControl h1 = new HtmlGenericControl("h1");
                    h1.InnerText = elem.Number.ToString() + "x " + elem.Name;
                    div.Controls.Add(h1);
                    HtmlGenericControl p = new HtmlGenericControl("p");
                    p.InnerText = "total: $" + (elem.Number * elem.Prize).ToString();
                    div.Controls.Add(p);
                    data.Controls.Add(div);
                    totalAmmount += elem.Number * elem.Prize;
                }
            }
        }

        protected void btBuy_Click(object sender, EventArgs e)
        {
            if(Session["user"] == null)
            {
                lbTitle.Text = "Log in to finalize your order";
            }
            else
            {
                string token = (new Random().Next(1000, 1000000)).ToString();
                MySqlConnection conn = DBCommands.conn();
                conn.Open();
                MySqlDataAdapter dAdapter = new MySqlDataAdapter();
                dAdapter.SelectCommand = new MySqlCommand("SELECT * FROM produkty WHERE 1", conn);
                DataTable dt = new DataTable();
                dAdapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    foreach (BasketElem elem in (List<BasketElem>)Session["basket"])
                    {
                        if (row.ItemArray[1].ToString() == elem.Name)
                        {
                            MySqlCommand comm = conn.CreateCommand();
                            comm.CommandText = "UPDATE produkty SET number='" + (Int16.Parse(row.ItemArray[3].ToString()) - elem.Number).ToString() + "' WHERE name='" + elem.Name + "'";
                            comm.ExecuteNonQuery();
                            comm.CommandText = "INSERT INTO orders(product,number,user,prize,token) VALUES(@prod,@num,@user,@prize,@token)";
                            comm.Parameters.AddWithValue("@prod", row.ItemArray[0].ToString());
                            comm.Parameters.AddWithValue("@num", row.ItemArray[3].ToString());
                            comm.Parameters.AddWithValue("@user", Session["user"]);
                            comm.Parameters.AddWithValue("@prize", totalAmmount);
                            comm.Parameters.AddWithValue("@token", Settings.CreateSHA(token));
                            comm.ExecuteNonQuery();
                        }
                    }
                }
                MySqlDataAdapter mailAdapter = new MySqlDataAdapter();
                mailAdapter.SelectCommand = new MySqlCommand("SELECT * FROM users WHERE name='" + Session["user"] + "'", conn);
                DataTable dtMail = new DataTable();
                mailAdapter.Fill(dtMail);
                foreach (DataRow rowMail in dtMail.Rows)
                {
                    MailMessage mail = new MailMessage("sklepnajlepszeassety@gmail.com", rowMail.ItemArray[2].ToString());
                    mail.Subject = "Order nr." + Settings.CreateSHA(token);
                    mail.Body = "Hello " + Session["user"] + ",\n\nThanks for buying on our website. You spend $" + totalAmmount + " for yours shopping.We hope that our products meet your expectations.\n\nBest regards sklepNajlepszeAsSety";
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("sklepnajlepszeassety@gmail.com", "N!mda@#$");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mail);
                }
                conn.Close();
                data.Controls.Clear();
                btBuy.Visible = false;
                lbTitle.Text = "Order successfully placed. Check your mail for more info.";
            }
           
        }
        protected void toLogin(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Session["user"] = null;
                btToLogin.Text = "Sign in|Sign out";
            }
            else
            {
                Response.Redirect("Form.aspx");
            }
        }
    }
}