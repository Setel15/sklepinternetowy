﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Item.aspx.cs" Inherits="sklepInternetowy.Item" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet"  href="bootstrap/vendor/bootstrap/css/bootstrap.min.css" />
    <style>
        #container{
            margin-top:250px;
            display:flex;
            flex-direction:row;
            height:100%;
            width:100%;
        }
        #images{
            float:left;
            width: 50%;
            display:flex;
            justify-content:center;
            align-content: center;
        }
        #itemContent{
            float:right;
            width:50%;
            text-align:center;
        }
        #expirience{
            justify-content: center;
            align-content: center;
            align-items: center;
            margin-top: 100px;
            display:flex;
            width: 100%;
            flex-direction:row;
        }
        #comments{
            margin-top: 150px;
            display:flex;
            width: 100%;
            flex-direction:column;
        }
        .comment{
            margin-top: 10px;
            border: 1px solid black;
            display:flex;
            width: 100%;
            flex-direction:column;
        }
        span{
            margin: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <div class="navbar-brand">Shop Name</div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.aspx">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForMen.aspx">For Men</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForWomen.aspx">For Women</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForChildren.aspx">For Children</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Sale.aspx">Sale</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Contact.aspx">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Basket.aspx">Basket</a>
                    </li>
                    <li class="nav-item">
                        <asp:Button CssClass="nav-link" BackColor="#343a40" ID="btToLogin" runat="server" OnClick="toLogin" Text="Sign in|Sign up" BorderStyle="None"></asp:Button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
        <div id="container">
            <div id="images">
                <asp:Image ID="BigImage" runat="server" />
            </div>
            <div id="itemContent">
                <h1><asp:Label ID="lbTitle" runat="server" Text="Title"></asp:Label></h1><br />
                <asp:Label ID="lbContent" runat="server" Text="Content"></asp:Label><br/>
                Price: $<asp:Label ID="lbCena" runat="server" Text="59,99"></asp:Label><br/>
                <asp:Label ID="lbLeft" runat="server" Text="100 items remain"></asp:Label> items remain <br/>
                User rating: <asp:Label ID="lbRating" runat="server" Text="5/5"></asp:Label><br/>
                Number: <asp:TextBox runat="server" ID="productsNumber"></asp:TextBox><br/>
                <asp:Button runat="server" OnClick="buy" Text="Add to basket"/>
            </div>
        </div>  
        <div id="expirience">
            Your rate:<asp:TextBox ID="tbRate" runat="server" Width="30px"></asp:TextBox>/5
            <pre>   </pre>Comment:<asp:TextBox ID="tbComment" runat="server" Width="500px"></asp:TextBox>
            <asp:Button ID="btAdd" runat="server" Text="Add" OnClick="btAdd_Click" />
            <br/><asp:Label ID="lbError" runat="server" Text=""></asp:Label>
        </div>
            <asp:Panel ID="comments" runat="server">
                <h2>Comments:</h2>
            </asp:Panel>
            
    </form>
    <script src="bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
