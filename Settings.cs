﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace sklepInternetowy
{
    public class Settings
    {
        public static String CreateSHA(string password)
        {
            SHA512 hash = SHA512.Create();
            byte[] bytes = hash.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
        public static void addSomeData()
        {
            MySqlConnection conn = DBCommands.conn();
            conn.Open();
            //string[] nazwy = {"Man","Woman","Kids","Sale"};
            //foreach(string opt in nazwy)
            //{
            //        MySqlCommand comm = conn.CreateCommand();
            //        comm.CommandText = "INSERT INTO produkty(name,price,number,tag,src) VALUES(@name,59.99,100,@tag,@src)";
            //        comm.Parameters.AddWithValue("@name", "blouse" + opt + "3");
            //        comm.Parameters.AddWithValue("@tag", opt);
            //        comm.Parameters.AddWithValue("@src", "blouse" + opt + "3.png");
            //        comm.ExecuteNonQuery();
            //}
            conn.Close();
        }
    }
}