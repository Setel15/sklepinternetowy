﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace sklepInternetowy
{
    public partial class Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void submit_Click(object sender, EventArgs e)
        {
            string name = tbName.Text;
            string email = tbMail.Text;
            string password = tbPassword.Text;
            string warning = "";
            lbWarning.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
            if (name.IndexOf("-") != -1 || email.IndexOf("--") != -1 || email.IndexOf(";-") != -1){
                warning = "Why want you to be naughty???";
            }
            else
            {
                MySqlConnection conn = DBCommands.conn();
                if (submit.Text == "Register")
                {
                    if (password.Length < 9)
                    {
                        warning += "Your Password should be longer than 8 charakters</br>";
                    }
                    if (name.Length == 0 || email.Length == 0)
                    {
                        warning += "Username and password can't be blank";
                    }
                    conn.Open();
                    var dAdapter = new MySqlDataAdapter();
                    dAdapter.SelectCommand = new MySqlCommand("SELECT * FROM users WHERE name='" + name + "'", conn);
                    DataTable dt = new DataTable();
                    dAdapter.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        warning += "Username is busy";
                    }
                    if (warning == "")
                    {
                        string hashPassword = Settings.CreateSHA("salt" + password);
                        for (int i = 0; i < 3; i++)
                        {
                            hashPassword = Settings.CreateSHA(i.ToString() + hashPassword);
                        }
                        string token = (new Random().Next(1, 100)).ToString();
                        MySqlCommand comm = conn.CreateCommand();
                        comm.CommandText = "INSERT INTO users(name, email, password, account,activated,token) VALUES(@name, @email,@password,0.00,0,@token)";
                        comm.Parameters.AddWithValue("@name", name);
                        comm.Parameters.AddWithValue("@email", email);
                        comm.Parameters.AddWithValue("@password", hashPassword);
                        comm.Parameters.AddWithValue("@token", Settings.CreateSHA(token));
                        comm.ExecuteNonQuery();
                        warning = "Account created! Please verify your e-mail before start using our site";
                        lbWarning.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00FF00");
                        MailMessage mail = new MailMessage("sklepnajlepszeassety@gmail.com", email);
                        mail.Subject = "E-mail Veryfication";
                        mail.Body = "Hello " + name + ",\n\nPlease click on the link bellow to verify your e-mail and activate your account\n<a href = 'http://localhost:44353/index.aspx/?token=" + token + "'>Click here to verify e-mail.</a>\n\nBest regards Sklep NajlepszeAsSety";
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential("sklepnajlepszeassety@gmail.com", "N!mda@#$");
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mail);
                    }
                }
                else
                {
                    string hashPassword = Settings.CreateSHA("salt" + password);
                    for (int i = 0; i < 3; i++)
                    {
                        hashPassword = Settings.CreateSHA(i.ToString() + hashPassword);
                    }
                    conn.Open();
                    var sda = new MySqlDataAdapter();
                    sda.SelectCommand = new MySqlCommand("SELECT * FROM users WHERE name='" + name + "' AND password='" + hashPassword + "' AND email='" + email + "' AND activated='1'", conn);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        Session["user"] = name;
                        Response.Redirect("index.aspx");
                    }
                    else
                    {
                        warning += "Something is wrong. Check if the entered data is correct and try again";
                    }
                }
                conn.Close();
            }
            lbWarning.Text = warning;
        }
        protected void btChange_Click(object sender, EventArgs e)
        {
            if (submit.Text == "Register")
            {
                submit.Text = "Login";
                btChange.Text = "Nie masz jeszcze konta? Utwórz teraz!";
            }
            else
            {
                submit.Text = "Register";
                btChange.Text = "Masz już konto? Zaloguj się";
            }
        }
    }
}