﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="sklepInternetowy.Form" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <link rel="stylesheet"  href="bootstrap/vendor/bootstrap/css/bootstrap.min.css" />
    <style>
        #container{
            left:30%;
            width:40%;
            position:absolute;
            top:20%;
        }
    </style>
</head>
<body>    
     <form id="form1" runat="server" class="text-center border border-light p-5">
         <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <div class="navbar-brand">Shop Name</div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.aspx">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
         <div id="container">
            Name: <asp:TextBox ID="tbName" runat="server" CssClass="form-control mb-4"></asp:TextBox>
            E-mail: <asp:TextBox ID="tbMail" runat="server" TextMode="Email" CssClass="form-control mb-4"></asp:TextBox>
            Password: <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" CssClass="form-control mb-4"></asp:TextBox>
            <asp:Button ID="submit" runat="server" Text="Register" OnClick="submit_Click"/>
             <asp:Button ID="btChange" runat="server" Width="100%" Text="Masz już konto? Zaloguj się" OnClick="btChange_Click" BorderStyle="None" BackColor="White" Font-Underline="True" ForeColor="Blue" />
             <asp:Label ID="lbWarning" runat="server" ForeColor="Red" ></asp:Label>
         </div>
    </form>
        <script src="bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
