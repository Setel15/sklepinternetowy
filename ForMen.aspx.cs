﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace sklepInternetowy
{
    public partial class ForMen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MySqlConnection conn = DBCommands.conn();
            conn.Open();
            if (Session["user"] == null)
            { }
            else if (Session["user"] != null)
            {
                btToLogin.Text = "Log out";
            }
            MySqlDataAdapter allDataAdapter = new MySqlDataAdapter();
            allDataAdapter.SelectCommand = new MySqlCommand("SELECT * FROM produkty WHERE tag='Man'", conn);
            DataTable allDt = new DataTable();
            allDataAdapter.Fill(allDt);
            foreach (DataRow row in allDt.Rows)
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["class"] = "col-lg-4 col-md-6 mb-4";
                HtmlGenericControl container = new HtmlGenericControl("div");
                container.Attributes["class"] = "card h-100 show";
                HtmlGenericControl img = new HtmlGenericControl("img");
                img.Attributes["src"] = "./assets/img/" + row.ItemArray[5].ToString();
                img.Attributes["class"] = "card-img-top";
                container.Controls.Add(img);
                HtmlGenericControl body = new HtmlGenericControl("div");
                body.Attributes["class"] = "card-body";
                HtmlGenericControl title = new HtmlGenericControl("h4");
                title.Attributes["class"] = "card-title";
                title.InnerText = row.ItemArray[1].ToString();
                body.Controls.Add(title);
                HtmlGenericControl price = new HtmlGenericControl("h5");
                price.InnerText = "$" + row.ItemArray[2].ToString();
                body.Controls.Add(price);
                HtmlGenericControl item = new HtmlGenericControl("p");
                item.InnerText = row.ItemArray[3].ToString() + " items left";
                body.Controls.Add(item);
                HtmlGenericControl description = new HtmlGenericControl("p");
                description.InnerText = row.ItemArray[6].ToString();
                description.Attributes["class"] = "card-text";
                body.Controls.Add(description);
                HtmlGenericControl stars = new HtmlGenericControl("small");
                stars.InnerText = "User rating: 5/5";
                body.Controls.Add(stars);
                container.Controls.Add(body);
                HtmlGenericControl footer = new HtmlGenericControl("div");
                footer.Attributes["class"] = "card-footer";
                HtmlGenericControl toItem = new HtmlGenericControl("a");
                toItem.Attributes["href"] = "Item.aspx?id=" + row.ItemArray[0].ToString();
                toItem.Attributes["style"] = "width:100%;text-align:center;";
                toItem.InnerText = "Show more";
                footer.Controls.Add(toItem);
                container.Controls.Add(footer);
                div.Controls.Add(container);
                panel.Controls.Add(div);
            }
            conn.Close();
        }
        protected void toLogin(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Session["user"] = null;
                btToLogin.Text = "Sign in|Sign out";
            }
            else
            {
                Response.Redirect("Form.aspx");
            }
        }
    }
}