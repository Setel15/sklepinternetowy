﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="sklepInternetowy.Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link rel="stylesheet"  href="bootstrap/vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet"  href="assets/css/index.css" />
    <title></title>
    <style>
        #form1{
            display:block;
            width:100%
        }
        #all{
            display:block
        }
        #hello{
            display:block;
            margin-bottom:25px
        }
        #to{
            width:50%;
            height:30px;
            border:2px solid black;
            display:block;
            margin-bottom:25px;
            margin-left:30px;
        }
        #nag {
            width: 50%;
            height: 30px;
            border: 2px solid black;
            display: block;
            margin-bottom: 25px;
            margin-left:30px;
        }
        #text{
            width: 50%;
            height: 300px;
            border: 2px solid black;
            display: block;
            margin-bottom: 35px;
            margin-left:30px;
        }
        span{
            margin-left:30px
        }
        button{
            width:100px;
            height:30px;
            margin-left:330px
        }
        #text{
            font-size: 20px;
        }
        #all{
            margin-top:150px;
            display: flex;
            justify-content: center;
            align-content: center;
            align-items: center;
            flex-direction: column;
        }
        input,textarea{
            width: 80%;
            margin-bottom: 20px;
        }
        textarea{
            height: 200px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <div class="navbar-brand">Shop Name</div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.aspx">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForMen.aspx">For Men</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForWomen.aspx">For Women</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ForChildren.aspx">For Children</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Sale.aspx">Sale</a>
                    </li>
                    
                    <li class="nav-item">
                        <asp:Panel ID="panelNav" runat="server" BackColor="#343a40"></asp:Panel>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="Contact.aspx">Contact<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Basket.aspx">Basket</a>
                    </li>
                    <li class="nav-item">
                        <asp:Button CssClass="nav-link" BackColor="#343a40" ID="btToLogin" runat="server" OnClick="toLogin" Text="Sign in|Sign up" BorderStyle="None"></asp:Button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="all">
            From:<asp:TextBox ID="tbFrom" runat="server"></asp:TextBox>
            Topic:<asp:TextBox id="tbTopic" runat="server"></asp:TextBox>
            Text:<asp:TextBox ID="tbText" runat="server" TextMode="MultiLine"></asp:TextBox>
            <asp:Button ID="btSend" runat="server" Text="Send" OnClick="btSend_Click"/>
        </div>
    </form>
    <script src="bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>