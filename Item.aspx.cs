﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace sklepInternetowy
{
    public partial class Item : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["id"] != null)
            {
                string id = Request.QueryString["id"].ToString();
                MySqlConnection conn = DBCommands.conn();
                conn.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = new MySqlCommand("SELECT name,content,price,number,src FROM produkty WHERE id='"+id+"'", conn);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                lbTitle.Text = dt.Rows[0].ItemArray[0].ToString();
                lbContent.Text = dt.Rows[0].ItemArray[1].ToString();
                lbCena.Text = dt.Rows[0].ItemArray[2].ToString();
                lbLeft.Text = dt.Rows[0].ItemArray[3].ToString();
                BigImage.ImageUrl ="./assets/img/" + dt.Rows[0].ItemArray[4].ToString();
                adapter.SelectCommand = new MySqlCommand("SELECT rate FROM glosy WHERE product='" + id + "'", conn);
                DataTable dtRate = new DataTable();
                adapter.Fill(dtRate);
                if(dtRate.Rows.Count > 0)
                {
                    int total = 0;
                    foreach (DataRow row in dtRate.Rows)
                    {
                        total += Int16.Parse(row.ItemArray[0].ToString());
                    }
                    lbRating.Text = (total / dtRate.Rows.Count).ToString() + "/5";
                }
                else
                {
                    lbRating.Text = "Nobody rate this product, you can be first";
                }
                
                adapter.SelectCommand = new MySqlCommand("SELECT user,text FROM komentarze WHERE product='" + id + "'", conn);
                DataTable dtCom = new DataTable();
                adapter.Fill(dtCom);
                foreach(DataRow row in dtCom.Rows)
                {
                    HtmlGenericControl div = new HtmlGenericControl("div");
                    div.Attributes["class"] = "comment";
                    HtmlGenericControl h1 = new HtmlGenericControl("h1");
                    h1.InnerText = row.ItemArray[0].ToString();
                    HtmlGenericControl p = new HtmlGenericControl("p");
                    p.InnerText = row.ItemArray[1].ToString();
                    p.Attributes["style"] = "text-align:center";
                    div.Controls.Add(h1);
                    div.Controls.Add(p);
                    comments.Controls.Add(div);
                }
                conn.Close();
            }
        }
        protected void buy(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"].ToString();
            string name = lbTitle.Text;
            int prodNum = Int16.Parse(productsNumber.Text);
            double cena = Double.Parse(lbCena.Text);
            if(prodNum < Int16.Parse(lbLeft.Text) && prodNum >= 1)
            {
                if (Session["basket"] == null)
                {
                    Session["basket"] = new List<BasketElem>();
                }
                List<BasketElem> basket = (List<BasketElem>)Session["basket"];
                basket.Add(new BasketElem(id, name, prodNum, cena));
                Session["basket"] = basket;
            }
        }

        protected void btAdd_Click(object sender, EventArgs e)
        {
            if(tbRate.Text =="")
            {
                lbError.Text = "Rate shouldn't be empty!";
            }else if(Int16.Parse(tbRate.Text) > 5 || Int16.Parse(tbRate.Text) < 1)
            {
                lbError.Text = "Rate out of range! Correct range is 1-5";
            }
            else if(tbComment.Text == "")
            {
                lbError.Text = "Comment shouldn't be empty!";
            }
            else
            {
                string user = "anon";
                if (Session["user"] != null) {
                    user = Session["user"].ToString();
                }
                    
                MySqlConnection conn = DBCommands.conn();
                conn.Open();
                MySqlCommand comm = conn.CreateCommand();
                comm.CommandText = "INSERT INTO komentarze(product,user,text) VALUES(@id,@user,@text)";
                comm.Parameters.AddWithValue("@id", Request.QueryString["id"].ToString());
                comm.Parameters.AddWithValue("@user", user);
                comm.Parameters.AddWithValue("@text", tbComment.Text);
                comm.ExecuteNonQuery();
                comm.CommandText = "INSERT INTO glosy(product,rate) VALUES(@id,@rate)";
                comm.Parameters.AddWithValue("@rate", tbRate.Text);
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }
        protected void toLogin(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Session["user"] = null;
                btToLogin.Text = "Sign in|Sign out";
            }
            else
            {
                Response.Redirect("Form.aspx");
            }
        }
    }
}