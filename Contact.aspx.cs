﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace sklepInternetowy
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void toLogin(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Session["user"] = null;
                btToLogin.Text = "Sign in|Sign out";
            }
            else
            {
                Response.Redirect("Form.aspx");
            }
        }

        protected void btSend_Click(object sender, EventArgs e)
        {
            if(tbFrom.Text != "" && tbTopic.Text != "" && tbText.Text != "")
            {
                MySqlConnection conn = DBCommands.conn();
                conn.Open();
                MySqlCommand comm = conn.CreateCommand();
                comm.CommandText = "INSERT INTO emails(author, topic, content,readed) VALUES(@author,@topic,@content,False)";
                comm.Parameters.AddWithValue("@author", tbFrom.Text);
                comm.Parameters.AddWithValue("@topic", tbTopic.Text);
                comm.Parameters.AddWithValue("@content", tbText.Text);
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}