﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace sklepInternetowy
{
    public partial class AdminPanel : System.Web.UI.Page
    {
        string which = "users";
        string tableName = "users";
        string[] optionsUsr = { "id", "name", "email", "account", "activated" };
        string[] optionsProd = { "id", "name", "price", "number", "tag", "src", "content" };
        string[] optionsOrd = { "id", "product", "number", "user","prize","token" };
        string[] options = new string[10];

        protected void remUser_Click(object sender, EventArgs e, string id, string table)
        {
            MySqlConnection conn = DBCommands.conn();
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM "+table+" WHERE id ='" + id + "'";
            comm.ExecuteNonQuery();
            conn.Close();
            Response.Redirect(Request.Url.ToString());
        }
        protected void showBase_Click(object sender, EventArgs e, string x)
        {
            which = x;
            Page_Load(sender, e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
            if (Session["user"].ToString() == "admin")
                {
                    panel.Controls.Clear();
                    LinkButton butUsers = new LinkButton();
                    butUsers.Click += (sender2, e2) => showBase_Click(sender2, e2, "users");
                    butUsers.Text = "Users";
                    panel.Controls.Add(butUsers);
                    LinkButton butProducts = new LinkButton();
                    butProducts.Click += (sender2, e2) => showBase_Click(sender2, e2, "products");
                    butProducts.Text = "Products";
                    panel.Controls.Add(butProducts);
                    LinkButton butOrders = new LinkButton();
                    butOrders.Click += (sender2, e2) => showBase_Click(sender2, e2, "orders");
                    butOrders.Text = "Orders";
                    panel.Controls.Add(butOrders);
                    lb404.Text = "Admin panel";
                    if (which == "users")
                    {
                        options = optionsUsr;
                        tableName = "users";
                    }
                    else if (which == "products")
                    {
                        options = optionsProd;
                        tableName = "produkty";
                    }
                    else if (which == "orders")
                    {
                        options = optionsOrd;
                        tableName = "orders";
                    }
                    MySqlConnection conn = DBCommands.conn();
                    conn.Open();
                    MySqlDataAdapter adapter = new MySqlDataAdapter();
                    adapter.SelectCommand = new MySqlCommand("SELECT " + string.Join(",", options) + " FROM " + tableName, conn);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    HtmlGenericControl table = new HtmlGenericControl("table");
                    table.Attributes["border"] = "1";
                    HtmlGenericControl th = new HtmlGenericControl("tr");
                    foreach (string opt in options)
                    {
                        HtmlGenericControl thc = new HtmlGenericControl("td");
                        thc.InnerText = opt;
                        th.Controls.Add(thc);
                    }
                    HtmlGenericControl del = new HtmlGenericControl("td");
                    del.InnerText = "Delete";
                    th.Controls.Add(del);
                    table.Controls.Add(th);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        HtmlGenericControl tr = new HtmlGenericControl("tr");
                        foreach (var item in dt.Rows[i].ItemArray)
                        {
                            HtmlGenericControl td = new HtmlGenericControl("td");
                            td.InnerText = item.ToString();
                            tr.Controls.Add(td);
                        }
                        HtmlGenericControl tda = new HtmlGenericControl("td");
                        LinkButton but = new LinkButton();
                        but.Text = "X";
                        string remId = dt.Rows[i][0].ToString();
                        but.Click += (sender2, e2) => remUser_Click(sender2, e2, remId, tableName);
                        tda.Controls.Add(but);
                        tr.Controls.Add(tda);
                        table.Controls.Add(tr);
                    }
                    panel.Controls.Add(table);
                } }
        }
    }
}